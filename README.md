# zwidoku

ZWI maker plugin for DokuWiki

## Introduction

This extension exports DokuWiki (https://www.dokuwiki.org/) articles to the ZWI file format (version 1.3) used by the Encyclosphere project of the Knowledge Standards Foundation (KSF). It allows downloading ZWI files with DokuWiki articles, as well as it can be used for automatic submissions of such files to the Encyclosphere network.  


## Installation

To install this plugin, go tto the DokuWiki install directory: 

```
cd lib/plugins/
git clone git@gitlab.com:ks_found/zwidoku.git
```


## Configuration

To configure this plugin, edit getEncyc.php after the line that starts as "start configuration".


S.Chekanov (KSF)  
