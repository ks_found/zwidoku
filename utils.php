<?php
/**
 * ZWIdoku Plugin: Utils 
 *
 * @license  LGPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @author   S.Chekanov 
 * @based_on "pageindex" plugin by Kite <Kite@puzzlers.org>
 * @based_on "externallink" plugin by Otto Vainio <plugins@valjakko.net>
 * @based_on "pagelist" plugin by Esther Brunner <wikidesign@gmail.com>
 *
 */


   /**
         * Returns a TeX compliant version of the specified reference.
         * @param filename The reference.
         * @return A TeX compliant version, with no spaces, and no weird char.
         */
        function texifyReference($reference) {
                $patterns[ 0] = '/[áâ�| åä]/ui';
                $patterns[ 1] = '/[ðéêèë]/ui';
                $patterns[ 2] = '/[íîìï]/ui';
                $patterns[ 3] = '/[óôòøõö]/ui';
                $patterns[ 4] = '/[úûùü]/ui';
                $patterns[ 5] = '/æ/ui';
                $patterns[ 6] = '/ç/ui';
                $patterns[ 7] = '/�~_/ui';
                $patterns[ 8] = '/\\s/';
                $patterns[ 9] = '/#/';
                $patterns[10] = '/[^A-Za-z0-9\\-:]/';
                $replacements[ 0] = 'a';
                $replacements[ 1] = 'e';
                $replacements[ 2] = 'i';
                $replacements[ 3] = 'o';
                $replacements[ 4] = 'u';
                $replacements[ 5] = 'ae';
                $replacements[ 6] = 'c';
                $replacements[ 7] = 'ss';
                $replacements[ 8] = '-';
                $replacements[ 9] = ':';
                $replacements[10] = '_';

                return preg_replace($patterns, $replacements, $reference);
        }


?>

