<?php
/**
 * Encycloed Plugin: Open the article.
 *
 * @license  LGPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @author   S.Chekanov 
 * @based_on "pageindex" plugin by Kite <Kite@puzzlers.org>
 * @based_on "externallink" plugin by Otto Vainio <plugins@valjakko.net>
 * @based_on "pagelist" plugin by Esther Brunner <wikidesign@gmail.com>
 *
 */


if(!defined('DOKU_INC')) define('DOKU_INC',realpath(dirname(__FILE__).'/../../../').'/');
if(!defined('DOKU_LIB')) define('DOKU_LIB',realpath(dirname(__FILE__).'/../../').'/');
if(!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_LIB.'plugins/');
//define('DOKU_MAIN',realpath(dirname(__FILE__).'/../../../').'/');

// must be run within Dokuwiki
if(!defined('DOKU_INC')) die("Cannot find installed DokuWiki");



//require_once(DOKU_INC.'/inc/pageutils.php');
require_once(DOKU_INC.'/inc/init.php');
require_once(DOKU_INC.'/inc/template.php');


///echo $_GET['p'];
//echo DOKU_MAIN;
//die();


    $CURRENT_URL="https://enhub.org/dokuwiki";

    $ID = cleanID(getID());
    $onlyCode = $INPUT->str('onlyCode');
    $insideTag = $INPUT->str('insideTag');

    if (empty($conf['useacl']) || auth_quickaclcheck($ID) >= AUTH_READ) {
        $file = rawWiki($ID);
        $fileLastMod = wikiFN($ID);
        $lastMod     = @filemtime($fileLastMod); // from page
        $NS=getNS($ID);

        $filepath = str_replace(":", "/", $ID);
        // list all revisions 
        $path="data/attic/". $filepath."*";
        $files = glob($path);


        $changes="data/meta/".$filepath .".changes";
        $indexed="data/meta/".$filepath .".indexed";
        $meta="data/meta/".$filepath .".meta";

        //$pagemeta=pageinfo();
        //print_r($pagemeta); 
        //echo "Open this article to edit: " . $ID . " NS:" . $NS ."  (not implemented) " . $lastMod;

  /*
  if ($_FILES["file"]["error"] > 0)
  {
  echo "Error: " . $_FILES["file"]["error"] . "<br>";
  }
else
  {
  echo "Upload: " . $_FILES["file"]["name"] . "<br>";
  echo "Type: " . $_FILES["file"]["type"] . "<br>";
  echo "Size: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
  echo "Stored in: " . $_FILES["file"]["tmp_name"];
  }
 */

$OpenS="/dokuwiki/lib/plugins/zwidoku/open_file.php";
$str = <<<EOD
<h2></h2>
<center>
<h2>Submit ZWI file</h2>
<form action="$OpenS" method="post" enctype="multipart/form-data">
 <input type="file" name="fileToUpload" id="fileToUpload" accept=".zwi" />
 <input type="submit" name="submit" value=" Upload ZWI file " />
</form>
</center>
EOD;

echo file_get_contents(DOKU_INC.'/lib/plugins/zwidoku/html_start');

if (!empty($_SERVER['REMOTE_USER'])) {
                                                                echo '<li class="nav-item nav-link"> ';
                                                                tpl_userinfo();
                                                                echo '</li>';
                                                                echo $str;
                                                             

} else {
  echo "<center><b>Please  <a href='/enhub.org/doku.php?id=start&do=login'> login to this editor</a> in order to upload a ZWI file.</b></center>";
} 


echo file_get_contents(DOKU_INC.'/lib/plugins/zwi_doku/html_end');






     } // end 









?>
