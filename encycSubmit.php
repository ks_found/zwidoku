<?php
/**
 * ZWIdoku Plugin: Submit the article.
 *
 * @license  LGPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @author   S.Chekanov 
 * @based_on "pageindex" plugin by Kite <Kite@puzzlers.org>
 * @based_on "externallink" plugin by Otto Vainio <plugins@valjakko.net>
 * @based_on "pagelist" plugin by Esther Brunner <wikidesign@gmail.com>
 *
 */


if(!defined('DOKU_INC')) define('DOKU_INC',realpath(dirname(__FILE__).'/../../../').'/');
if(!defined('DOKU_LIB')) define('DOKU_LIB',realpath(dirname(__FILE__).'/../../').'/');
if(!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_LIB.'plugins/');
//define('DOKU_MAIN',realpath(dirname(__FILE__).'/../../../').'/');

// must be run within Dokuwiki
if(!defined('DOKU_INC')) die("Cannot find installed DokuWiki");



//require_once(DOKU_INC.'/inc/pageutils.php');
require_once(DOKU_INC.'/inc/init.php');
require_once(DOKU_INC.'/inc/template.php');


    $ID = cleanID(getID());
    $onlyCode = $INPUT->str('onlyCode');
    $insideTag = $INPUT->str('insideTag');

    if (empty($conf['useacl']) || auth_quickaclcheck($ID) >= AUTH_READ) {
        $file = rawWiki($ID);
        $fileLastMod = wikiFN($ID);
        $lastMod     = @filemtime($fileLastMod); // from page
        $NS=getNS($ID);

        $filepath = str_replace(":", "/", $ID);
        // list all revisions 
        $path="data/attic/". $filepath."*";
        $files = glob($path);


        $changes="data/meta/".$filepath .".changes";
        $indexed="data/meta/".$filepath .".indexed";
        $meta="data/meta/".$filepath .".meta";

        echo "Submit article: " . $ID . " NS:" . $NS ."  (not implemented) " . $lastMod;


     }









?>
